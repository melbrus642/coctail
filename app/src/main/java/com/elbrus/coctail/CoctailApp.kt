package com.elbrus.coctail

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.room.RoomDatabase
import com.elbrus.coctail.api.ApiFactory
import com.elbrus.coctail.api.repository.MainRepository
import com.elbrus.coctail.db.DrinkDatabase

class CoctailApp : Application() {


    companion object {
        lateinit var context: Context
        lateinit var repository: MainRepository
        lateinit var apiFactory: ApiFactory
        lateinit var dataBase : DrinkDatabase

        val historyData : MutableLiveData<Boolean> by lazy {
            MutableLiveData<Boolean>()
        }
    }


    override fun onCreate() {
        super.onCreate()

        context = applicationContext
        apiFactory = ApiFactory(context)
        repository = MainRepository(apiFactory.apiInterface)


        dataBase = Room.databaseBuilder(
            this,
            DrinkDatabase::class.java,
            "drink.db"

        ).fallbackToDestructiveMigration()
            .build()

    }
}