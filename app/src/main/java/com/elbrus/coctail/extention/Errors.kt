package com.elbrus.coctail.extention



    fun getError(code : Int) : String {
        return when (code) {
            400 -> "Request had bad syntax or was impossible to fulfill"
            401 -> "Unauthorised"
            404 -> "Not found"
            500 -> "Server error"
            501 -> "Not Implemented"
            else -> "Something went wrong"
        }
    }
