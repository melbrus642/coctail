package com.elbrus.coctail.api.repository

import com.elbrus.coctail.api.ApiInterface
import com.elbrus.coctail.base.BaseModel
import com.elbrus.coctail.model.DrinkRandomResponse


@Suppress("UNCHECKED_CAST")
class MainRepository(var api: ApiInterface) : BaseRepository() {

    suspend fun getCoctail(): BaseModel<DrinkRandomResponse> {
        return safeApiCall { api.getCoctail().await() } as BaseModel<DrinkRandomResponse>
    }

}