package com.elbrus.coctail.api.repository

import com.elbrus.coctail.base.BaseModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import retrofit2.Response
import java.lang.reflect.Type

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): Any? {

        val result : Result<T> = safeApiResult(call)
        var data : Any? = null

        data = when(result) {
            is Result.Success ->
                result.data
            is Result.Error -> {
                result.data
            }
        }


        return data

    }

    private suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>) : Result<T> {
        val response = call.invoke()
        if(response.isSuccessful && response.body() != null){
            val baseModel : BaseModel<T> = BaseModel()
            baseModel.data = response.body()
            baseModel.isSuccess = response.isSuccessful
            baseModel.code = response.code()
            return Result.Success(baseModel)
        }


        val baseModel : BaseModel<T> = BaseModel()
        baseModel.data = null
        baseModel.isSuccess = response.isSuccessful
        baseModel.code = response.code()

        return Result.Error(baseModel)
    }
}