package com.elbrus.coctail.db

import androidx.room.TypeConverter
import java.util.*

class DateConverter {

    @TypeConverter
    fun toRaw(date: Date?): Long? {
        if(date == null) return null

        return date.time
    }

    @TypeConverter
    fun fromRaw(timestamp: Long?): Date? {
        return timestamp?.let { Date(it) }
    }

}