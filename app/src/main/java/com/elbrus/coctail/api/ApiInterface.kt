package com.elbrus.coctail.api

import com.elbrus.coctail.base.BaseModel
import com.elbrus.coctail.model.DrinkRandomResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET


interface ApiInterface {

    @GET("api/json/v1/1/random.php")
    fun getCoctail() : Deferred<Response<DrinkRandomResponse>>


}