package com.elbrus.coctail.ui.adapter.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.elbrus.coctail.databinding.HistoryItemBinding
import com.elbrus.coctail.model.Drink
import com.elbrus.coctail.ui.adapter.HistoryRvAdapter

class HistoryHolder(private var binding : HistoryItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun onBind(drink: Drink, listener : HistoryRvAdapter.Listener, position : Int){

        binding.drink = drink

        binding.deleteButton.setOnClickListener{
            listener.onDelete(drink, position)
        }


    }

}