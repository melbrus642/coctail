package com.elbrus.coctail.ui.view_model


import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.elbrus.coctail.CoctailApp.Companion.dataBase
import com.elbrus.coctail.base.BaseViewModel
import com.elbrus.coctail.model.Drink
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HistoryViewModel : BaseViewModel() {

    var isEmptyHistory = ObservableField(false)

    val drinksData : MutableLiveData<ArrayList<Drink>> by lazy {
        MutableLiveData<ArrayList<Drink>>()
    }

    fun getHistoryDrinks() {
        scope.launch {
            val drinks : ArrayList<Drink> = ArrayList()
            dataBase.drinkDao().getAll().map {
                drinks.add(it)
            }


            setDrinksData(drinks)
        }
    }


    private suspend fun setDrinksData(drinks : ArrayList<Drink>) : LiveData<ArrayList<Drink>>{
        withContext(Dispatchers.Main){
            drinksData.value = drinks
        }

        return drinksData
    }


    fun deleteFromHistory(drink: Drink){
        scope.launch {
            dataBase.drinkDao().delete(drink)
        }
    }

}