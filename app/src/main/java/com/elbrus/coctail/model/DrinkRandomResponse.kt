package com.elbrus.coctail.model

import com.google.gson.annotations.SerializedName

data class DrinkRandomResponse(
    @SerializedName("drinks") var drinks : ArrayList<Drink>? = null
)