package com.elbrus.coctail.ui


import androidx.viewpager.widget.ViewPager
import com.elbrus.coctail.R
import com.elbrus.coctail.base.BaseActivity
import com.elbrus.coctail.databinding.ActivityMainBinding
import com.elbrus.coctail.ui.adapter.PagerAdapter

class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main) {


    override fun setupView() {
        binding!!.viewPager.adapter = PagerAdapter(supportFragmentManager)
        binding!!.viewPager.offscreenPageLimit = 5
        binding!!.viewPager.isEnabled = false
        binding!!.viewPager.disableScroll(true)

        binding!!.bottomNav.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_home -> binding!!.viewPager.setCurrentItem(0, false)
                R.id.nav_history -> binding!!.viewPager.setCurrentItem(1, false)
            }

            true
        }
    }

}