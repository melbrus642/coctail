package com.elbrus.coctail.db

import androidx.room.*
import com.elbrus.coctail.model.Drink


@Dao
interface DrinkDao {
    @Query("SELECT * FROM drink")
    fun getAll(): List<Drink>

    @Insert
    fun insert(vararg drink: Drink)

    @Delete
    fun delete(drink: Drink)
}