package com.elbrus.coctail.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.elbrus.coctail.db.DateConverter
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Drink(
    @PrimaryKey(autoGenerate = true)
    var id : Int,
    @SerializedName("idDrink")
    var idDrink : String? = null,
    @SerializedName("strDrink")
    @ColumnInfo(name = "strDrink")
    var strDrink : String? = null,
    @SerializedName("strInstructions")
    @ColumnInfo(name = "strInstructions")
    var strInstructions : String? = null,
    @SerializedName("strInstructionsDE")
    @ColumnInfo(name = "strInstructionsDE")
    var strInstructionsDE : String? = null,
    @SerializedName("strInstructionsIT")
    @ColumnInfo(name = "strInstructionsIT")
    var strInstructionsIT : String? = null,
    @SerializedName("strDrinkThumb")
    @ColumnInfo(name = "strDrinkThumb")
    var strDrinkThumb : String? = null,
    @TypeConverters(DateConverter::class)
    var dateModified : Date? = null
)