package com.elbrus.coctail.ui.view_model

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.elbrus.coctail.CoctailApp
import com.elbrus.coctail.CoctailApp.Companion.dataBase
import com.elbrus.coctail.CoctailApp.Companion.repository
import com.elbrus.coctail.base.BaseModel
import com.elbrus.coctail.base.BaseViewModel
import com.elbrus.coctail.model.Drink
import com.elbrus.coctail.model.DrinkRandomResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class HomeViewModel : BaseViewModel() {

    var isLoad = ObservableField(false)

    val drinkData : MutableLiveData<BaseModel<DrinkRandomResponse>> by lazy {
        MutableLiveData<BaseModel<DrinkRandomResponse>>()
    }


    fun getCoctail(){
        scope.launch {
            try {
                isLoad.set(true)
                val drink = repository.getCoctail()
                setDrink(drink)
                isLoad.set(false)

            }catch (e : Exception){
                e.stackTrace
                isLoad.set(false)
            }
        }
    }


    private suspend fun setDrink(response: BaseModel<DrinkRandomResponse>) : LiveData<BaseModel<DrinkRandomResponse>>{
        withContext(Dispatchers.Main){
            drinkData.value = response
        }

        return drinkData
    }


     fun saveToDb(drink: Drink){
        scope.launch {
            dataBase.drinkDao().insert(drink)

            if (dataBase.drinkDao().getAll().size > 10){
                dataBase.drinkDao().delete(dataBase.drinkDao().getAll()[0])
            }
        }
    }
}