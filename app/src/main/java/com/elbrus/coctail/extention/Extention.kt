package com.elbrus.coctail.extention

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.elbrus.coctail.R

fun Context.showToast(toastMessage: String?) {
    Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show()
}


fun Activity.showMessageDialog(
    content: CharSequence?,
    isDone: Boolean = false,
    onOkListener: () -> Unit = { }
): Dialog {
    val builder = AlertDialog.Builder(this, R.style.MyDialogStyle)
    val inflater: LayoutInflater = layoutInflater
    val dialogView: View = inflater.inflate(R.layout.dialog_message, null)
    builder.setView(dialogView)
    builder.setCancelable(false)

    val title: TextView = dialogView.findViewById(R.id.dialog_title)
    val icon: ImageView = dialogView.findViewById(R.id.dialog_icon)
    val confirm: Button = dialogView.findViewById(R.id.confirm_button)


    val dialog = builder.create()
    title.text = content
    if (isDone) {
        icon.setImageResource(R.drawable.ic_done_dialog)
    } else {
        icon.setImageResource(R.drawable.ic_warning)
    }
    confirm.setOnClickListener {
        dialog.dismiss()
        onOkListener()
    }


    dialog.show()
    return dialog
}

fun Activity.showConfirmDialog(
    content: CharSequence,
    positiveBtnId: Int = R.string.yes,
    negativeBtnId: Int = R.string.no,
    onOkListener: () -> Unit = { }
): Dialog {
    val builder = AlertDialog.Builder(this, R.style.MyDialogStyle)
    val inflater: LayoutInflater = layoutInflater
    val dialogView: View = inflater.inflate(R.layout.dialog_confirm, null)
    builder.setView(dialogView)
    builder.setCancelable(false)

    val title: TextView = dialogView.findViewById(R.id.dialog_title)
    val confirm: TextView = dialogView.findViewById(R.id.confirm_button)
    val reject: TextView = dialogView.findViewById(R.id.not_confirm_button)


    val dialog = builder.create()
    title.text = content
    confirm.setOnClickListener {
        onOkListener()
        dialog.dismiss()
    }
    confirm.setText(positiveBtnId)
    reject.setOnClickListener { dialog.dismiss() }
    reject.setText(negativeBtnId)

    dialog.show()
    return dialog
}