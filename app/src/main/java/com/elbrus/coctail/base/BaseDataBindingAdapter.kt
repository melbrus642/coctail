package com.elbrus.coctail.base


import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.elbrus.coctail.R
import java.text.SimpleDateFormat
import java.util.*




@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, url: String?) {
    val circularProgressDrawable = CircularProgressDrawable(imageView.context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    if (!url.isNullOrEmpty()) {
        Glide.with(imageView.context).load(url).placeholder(circularProgressDrawable)
            .into(imageView)
    } else {
        Glide.with(imageView.context).load(url).placeholder(R.drawable.image_placeholder).into(imageView)
    }

}


@BindingAdapter("loadDate", "dateFormat")
fun setDate(textView : TextView, date: Date?, dateFormat : String) {
    if(date == null){
        textView.text = ""
    } else{
        textView.text = SimpleDateFormat(dateFormat).format(date)
    }
}


@BindingAdapter("isRefresh")
fun setRefresh(refreshLayout : SwipeRefreshLayout, isRefresh : Boolean){
    refreshLayout.isRefreshing = isRefresh
}


