package com.elbrus.coctail.api

import android.content.Context
import com.elbrus.coctail.BuildConfig
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.TimeUnit


class ApiFactory(var context: Context) {

    private val BASE_URL : String = "https://www.thecocktaildb.com/"
    private lateinit var retrofit: Retrofit



    private fun getRetrofitClient(): Retrofit? {
            val cookieManager = CookieManager()
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
            val logging = HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            }

        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create()

            val oktHttpClient = OkHttpClient.Builder()
                .connectTimeout(
                    30,
                    TimeUnit.MINUTES
                )
                .writeTimeout(30, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.MINUTES)
                .addInterceptor(ConnectivityInterceptor(context))
                .cookieJar(JavaNetCookieJar(cookieManager))
        oktHttpClient.addInterceptor(logging)

            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(oktHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

        return retrofit
    }



    val apiInterface : ApiInterface = getRetrofitClient()!!.create(ApiInterface::class.java)
}

