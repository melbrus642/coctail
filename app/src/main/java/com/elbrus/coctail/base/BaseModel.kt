package com.elbrus.coctail.base


data class BaseModel<T>(
    var isSuccess: Boolean? = null,
    var data: T? = null,
    var errorMessage: String? = null,
    var code: Int? = null
)