package com.elbrus.coctail.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.elbrus.coctail.R
import com.elbrus.coctail.databinding.HistoryItemBinding
import com.elbrus.coctail.model.Drink
import com.elbrus.coctail.ui.adapter.viewHolder.HistoryHolder

class HistoryRvAdapter(var items : ArrayList<Drink>, var listener : Listener) : RecyclerView.Adapter<HistoryHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        val binding : HistoryItemBinding? = DataBindingUtil.bind(LayoutInflater.from(parent.context).inflate(
            R.layout.history_item, parent, false))

        return HistoryHolder(binding!!)
    }

    override fun getItemCount(): Int {
       return items.size
    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {
        holder.onBind(items[position], listener, position)
    }


    interface Listener {
        fun onDelete(drink: Drink, position: Int)
    }
}