package com.elbrus.coctail.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.elbrus.coctail.ui.fragment.HistoryFragment
import com.elbrus.coctail.ui.fragment.HomeFragment

class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) HomeFragment() else HistoryFragment()
    }

    override fun getCount(): Int {
        return 2
    }
}