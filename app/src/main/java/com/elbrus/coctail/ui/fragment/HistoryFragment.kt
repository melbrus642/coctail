package com.elbrus.coctail.ui.fragment

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.elbrus.coctail.CoctailApp.Companion.historyData
import com.elbrus.coctail.R
import com.elbrus.coctail.base.BaseFragment
import com.elbrus.coctail.databinding.HistoryFragmentBinding
import com.elbrus.coctail.extention.showConfirmDialog
import com.elbrus.coctail.model.Drink
import com.elbrus.coctail.ui.adapter.HistoryRvAdapter
import com.elbrus.coctail.ui.view_model.HistoryViewModel
import com.elbrus.coctail.utils.RecyclerAnimation

class HistoryFragment : BaseFragment<HistoryFragmentBinding>(R.layout.history_fragment), HistoryRvAdapter.Listener {


    private var viewModel: HistoryViewModel? = null
    private var drinks : ArrayList<Drink> = ArrayList()
    private lateinit var adapter: HistoryRvAdapter


    override fun setupView() {
        if (viewModel == null){
            viewModel = ViewModelProvider(this).get(HistoryViewModel::class.java)
            binding!!.viewModel = viewModel

            viewModel!!.getHistoryDrinks()

            binding!!.refreshLayout.setOnRefreshListener {
                viewModel!!.getHistoryDrinks()
                binding!!.refreshLayout.isRefreshing = false
            }

            viewModel!!.drinksData.observe(this, Observer {
                setupHistoryRecycler(it)
            })

            historyData.observe(this, Observer {
                viewModel!!.getHistoryDrinks()
            })

        }

    }


    private fun setupHistoryRecycler(drinks1 : ArrayList<Drink>){
        this.drinks = drinks1
        adapter = HistoryRvAdapter(drinks, this)
        binding!!.historyRecycler.adapter = adapter
        RecyclerAnimation.startAnimation(binding!!.historyRecycler, R.anim.main_recycler_anim_layout)

        viewModel!!.isEmptyHistory.set(drinks.isEmpty())
    }

    override fun onDelete(drink: Drink, position: Int) {
        requireActivity().showConfirmDialog("Удалить ${drink.strDrink}?"){
            viewModel!!.deleteFromHistory(drink)
            drinks.remove(drink)
            adapter.notifyItemRemoved(position)
            adapter.notifyItemRangeChanged(position, adapter.itemCount)
            viewModel!!.isEmptyHistory.set(drinks.isEmpty())
        }

    }

}