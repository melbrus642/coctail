package com.elbrus.coctail.ui.fragment

import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.elbrus.coctail.CoctailApp
import com.elbrus.coctail.CoctailApp.Companion.historyData
import com.elbrus.coctail.R
import com.elbrus.coctail.base.BaseFragment
import com.elbrus.coctail.databinding.HomeFragmentBinding
import com.elbrus.coctail.extention.getError
import com.elbrus.coctail.extention.showMessageDialog
import com.elbrus.coctail.extention.showToast
import com.elbrus.coctail.model.Drink
import com.elbrus.coctail.ui.view_model.HomeViewModel

class HomeFragment : BaseFragment<HomeFragmentBinding>(R.layout.home_fragment) {

    private var viewModel : HomeViewModel? = null

    override fun setupView() {
        if (viewModel == null){
            viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
            binding!!.viewModel = viewModel

            viewModel!!.drinkData.observe(this, Observer {
                if (it.data != null){
                    setupUi(it.data!!.drinks)
                }else{
                    requireActivity().showMessageDialog(getError(it.code!!)){
                        viewModel!!.getCoctail()
                    }
                }
            })


            binding!!.refreshLayout.setOnRefreshListener {
                viewModel!!.getCoctail()
            }

            viewModel!!.getCoctail()
        }

    }


    private fun setupUi(drinks : ArrayList<Drink>?){
        if (!drinks.isNullOrEmpty()){
            val drink = drinks[0]
            binding!!.drink = drink
            viewModel!!.saveToDb(drink)
            historyData.value = true
        }
    }

}