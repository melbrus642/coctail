package com.elbrus.coctail.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.elbrus.coctail.model.Drink


@Database(entities = [Drink::class], version = 1)
@TypeConverters(DateConverter::class)
abstract class DrinkDatabase : RoomDatabase() {
    abstract fun drinkDao(): DrinkDao
}